/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//Usando una estructura Mientras (while), realizar un algoritmo que escriba los números de uno en uno hasta 20
package tarea2sem2_melina;

/**
 *
 * @author Melina Gomez
 */
public class NumeroHasta20 {
    public void numero() {
        int i;
        i = 1;
        
        while (i <=20){
            System.out.println(i);
            i = i + 1;
        }
        
        
    }
}
