/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
// Desarrolle un algoritmo que permita calcular el promedio de notas de los exámenes de un estudiante.
// El algoritmo solo permite ingresar una nota a la vez.
// Considere que se realizan únicamente tres exámenes (nota1, nota2, nota3).
package tarea2sem2_melina;

import java.util.Scanner;

/**
 *
 * @author Melina Gomez
 */
public class PromedioNotas {
    public void promedio() {
        int i = 1;
        int nota;
        float promedio = 0;
        
        while(i <= 3){
            Scanner var = new Scanner(System.in);
            System.out.println("Digite la nota: ");
            nota = var.nextInt();
            promedio = promedio + nota;
            i = i + 1;
        }
        promedio = promedio / 3;
        System.out.println("El promedio es: " + promedio);
    }
    
}
