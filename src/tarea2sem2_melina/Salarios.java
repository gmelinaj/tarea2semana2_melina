/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//# Se requiere un algoritmo para calcular la planilla de una persona
//# Salario bruto, salario neto, teniendo en consideración que al empleado se le deduce el 9.17% de su salario como cargas sociales.
//# Para ello se dispone de las horas laboradas por mes y el monto por hora.
//# Debe mostrar el salario bruto, el salario neto y el monto de las deducciones.
package tarea2sem2_melina;

import java.util.Scanner;

/**
 *
 * @author Melina Gomez
 */
public class Salarios {
    public void salario() {
        int horasLaboradas, precioHora;
        float salarioBruto, deducciones;
        float salarioNeto;
        Scanner var = new Scanner(System.in);
        System.out.println("Digite la cantidad de horas laboradas en el mes: ");
        horasLaboradas = var.nextInt();
        System.out.println("Digite el precio por hora: ");
        precioHora = var.nextInt();
        
        salarioBruto =horasLaboradas * precioHora;
        salarioNeto =  (float) (salarioBruto - (salarioBruto *(9.17 /100)));
        deducciones =  (salarioBruto - salarioNeto);
        
        System.out.println("El salario bruto es: " + salarioBruto + " El salario neto es: " + salarioNeto + "Las deducciones son: " + deducciones);
    }
}
