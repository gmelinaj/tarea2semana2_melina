/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea2sem2_melina;

import java.util.Scanner;

/**
 *
 * @author Melina Gomez
 */
public class Tarea2sem2_Melina {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        int opcion;
        Scanner sc = new Scanner(System.in);
        System.out.println("-------Menú-------");
        System.out.println("1.Edad Actual");
        System.out.println("2.Salarios");
        System.out.println("3.Numeros Hasta 20");
        System.out.println("4.Promedio Notas");
        System.out.print("Opcion: ");
        opcion = sc.nextInt();
        switch (opcion) {
            case 1:
                EdadActual ea = new EdadActual();
                ea.edad();
                break;
            case 2:
                Salarios sal = new Salarios();
                sal.salario();
                break;
            case 3:
                NumeroHasta20 nh = new NumeroHasta20();
                nh.numero();
                break;
            case 4:
                PromedioNotas pn = new PromedioNotas();
                pn.promedio();
                break;
                
            default:
                System.out.printf("Opcion no valida");
                break;
    }
        
    }
    
}
